###############################################################
# analyze the big_sector_list dataframe created by
#   make-big-sector-list-with-old-and-new-terras.R
# in order to figure out which of the NEW {geobr} terras
# do not have ethnic information (yet)
###############################################################

rm(list=ls())

library(tidyverse)
library(sf)
library(geobr)

load("../Data/big-sector-list.Rdata")

count_has = function(L) { sum( sapply(L,length)>0 ) }
count_no  = function(L) { sum( sapply(L,length)==0 ) }

x = big_sector_list %>%
      filter(!is.na(gid_new)) %>%
      group_by(name_new, gid_new, inew, code_terrai) %>%
      summarize(nsectors=n(),
                C = sum(C0,na.rm=TRUE),
                W = sum(F15,F20,F25,F30,F35,F40,F45, na.rm=TRUE),
                UF = paste( unique(unlist(states)) , collapse=','),
                has_povo   = count_has(povos),
                no_povo    = count_no(povos),
                has_lingua = count_has(linguas),
                no_lingua  = count_no(linguas),
                has_family = count_has(families),
                no_family  = count_no(families),
                has_trunk  = count_has(trunks),
                no_trunk   = count_no(trunks)
                )

need_ethnic_info = x %>%
                     ungroup() %>%
                     filter(has_povo==0 |
                            has_lingua==0 |
                            has_family==0 |
                            has_trunk==0) %>%
                     select(name=name_new,
                            UF,
                            gid_new,
                            nsectors,
                            Children=C,
                            Women=W) 
                   

# make a pdf file with maps of the TIs that need info

if (!exists('state_sf')) {
state_sf = read_state(code_state='all')
}

if (!exists('TI_sf')) {
  TI_sf = read_indigenous_land(date=201907) %>%
            filter( gid %in% need_ethnic_info$gid_new)
}

# merge the {geobr} etnia_nome column on for Marta's inspection

etn = tibble( gid_new = pull(TI_sf, gid),
              etnia_Funai = pull(TI_sf, etnia_nome))

need_ethnic_info = need_ethnic_info %>%
                    left_join(etn) %>%
                    transform(povos='--',linguas='--',families='--',trunks='--')

write.csv(need_ethnic_info, file='../Data/need-ethnic-info.csv',
          row.names = FALSE)

# make a pdf file with maps of the TIs that need info

pdf('../Data/need-ethnic-info.pdf')

   for (i in 1:nrow(TI_sf)) {
     
     this_uf = filter(state_sf, code_state == TI_sf$code_state[i])
     this_ti = TI_sf[i,]
     
     G = ggplot() +
           geom_sf(data=this_uf, fill=NA) +
           geom_sf(data=this_ti, fill='red', color='grey',lwd=0.1) +
           geom_sf(data=st_centroid(this_ti), size=10, color='red', fill=NA,shape=1) +
           labs(title=paste0(this_uf$abbrev_state,
                             ': ', 
                             this_ti$terrai_nom,
                             ' (gid= ', this_ti$gid, ')' ),
                subtitle=paste('FUNAI etnia=',this_ti$etnia_nome)) +
          theme_bw() 
            
     print(G)
   }

dev.off()



