#############################################
# make two sf data frames 
# one for old TI file, one for new geobr version
#
# each has polygons, and a list of 
# included sectors
#############################################

graphics.off()

library(sf)
library(tidyverse)
library(geobr)

# OLD TI ----

load("../Data/AA-data-for-Stan-analysis.Rdata")

old_setores = read.csv('../Data/setores.indigenas.csv') %>%
           mutate(code = as.character(setorcode))

x = sapply(unique(TIpop$GID0), function(id) { 
     list(filter(old_setores,GID0==id)$code)
    })

TI_old = st_read('C:/Users/User/Dropbox/IUSSP Indig Fert Paper/Data and Programs/Data/shapefiles/indi2010.shp') %>%
  mutate(terra_name = iconv(NOME_TI23,from='IBM437', to='UTF-8')) %>%
  filter(GID0 %in% TIpop$GID0) %>%
  add_column(sectors=x) %>%
  select(gid=GID0,terra_name,sectors,geometry)

# NEW TI ----

if (!exists('TI_new')){
  TI_new = read_indigenous_land(date=201907)
}

TI_old = st_buffer(TI_old, dist=0)
TI_new = st_buffer(TI_new, dist=0)

# common projection
TI_old = st_transform(TI_old, crs = st_crs(TI_new))

# read the entire sector file (300,000+ rows)
# calculate spatial overlaps with new TIs

if (!exists('all_setores')) {
    all_setores = read_census_tract(code_tract = 'all')

    io  =  st_overlaps(TI_new, all_setores ) 
    ic  =  st_contains(TI_new, all_setores )
 
    # a list (1 item per new TI)
    # of sectors that either overlap or are
    # contained by TI boundaries
    
    ix = lapply(seq(io), function(k) unique(unlist(c(io[k],
                                                     ic[k]))) ) 
}

# only keep the sectors that either overlap or are contained in TIs



# find all the sectors that overlap, intersect, or
# are contained by the terras in the new shapefile

L = list()

pdf(file='../Data/make-sector-lists-for-old-and-new-TI-shapefiles.pdf')

for (k in seq(TI_new$gid)) {

  print(paste(k,'/',nrow(TI_new)))
  
  sel = ix[[k]]
  
  if (length(sel) > 0) {
  
  # fraction of sector areas within
  # the selected TI
  
  denom = sapply(sel, function(i) st_area(all_setores[i,]))
  numer = sapply(sel, function(i) st_area(st_intersection(TI_new[k,], all_setores[i,])))
  
  tmp = all_setores[sel,] %>%
          add_column(denom, numer) %>%
          mutate(pct = numer/denom,
                 keep = (pct > .50))
  
  L[[k]] = filter(tmp,keep)$code_tract
  
  G =   ggplot() +
    geom_sf(data=tmp, aes(fill=keep)) +
    geom_sf_text(data=tmp,label=round(100*tmp$pct),size=3) +
    geom_sf(data=TI_new[k,], fill=NA, color='blue', lwd=2, alpha=.50) +
    geom_sf(data=tmp, fill=NA,color='white', lwd=.50) +
    geom_sf_text(data=tmp,label=round(100*tmp$pct),size=3) +
    labs(title=paste('TI_new[',k,']\n',TI_new[k,'terrai_nom'],'/',TI_new[k,'fase_ti']))
  
  # 
 print(G)
 
  } # if length(sel)>0

} # for k

dev.off()

for (i in 1:nrow(TI_old)) {
    TI_new$sectors = all_setores$code_tract[ L[[i]] ]
}

# write out the 2 sector lists 

TI_sectors = 
  list( old = select(TI_old, old_gid=gid, terra_name, sectors) %>%
                st_set_geometry(NULL),
        
        new = select(TI_new, new_gid=gid, code_terrai, terra_name=terrai_nom ) %>%
                 st_set_geometry(NULL)
        ) 

save( TI_old, TI_new, TI_sectors, 
      file='../Data/sector-lists-for-old-and-new-TI-shapefiles.Rdata')


