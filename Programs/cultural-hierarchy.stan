data {
  int n;       // number of terras
  int nt;      // number of linguistic trunks and effects (probably 4)
  int nf;      // number of linguistic families (probably 77)
  int nx;      // number of local covariates (initially one:literacy)

  vector[n]  local_x;

  int C[n];           // number of children 0-4 (both sexes)
  matrix[n,7] W;      // number of women 15-49
  matrix[n,2] QAB;    // prior will be q(5) ~ beta( QAB[,1],  QAB[,2]), by terra

  matrix[nt+nf+n,n]  K;  // BASIS FUNCTIONS for (trunk,family,terra) effects

  matrix[4,11] wilmothABCV;   // Wilmoth mortality coefs for 11 ages x=0,1,5,10,...,45
                              // (a coefs on row 1, b on 2, ... )  

  matrix[11,12] cs_constants;     // constants for cumulative sums (mx * cs_contants) is cumul haz
  matrix[12,10] trapez_constants; // constants for trapezoidal approx of Lx from lx

  matrix[n,7]   M;    // each row = empirical mean of gamma in the (HFD+IDB) sample of schedules (repeated n times)
  matrix[2,7]   X;    // rows contain largest 2 principal components of (gamma - mu_gamma) = beta %*% X,
                      // scaled so that beta ~ iid N(0, 1)

  real include_x;

}    

transformed data {
  vector[n] xdiff;
  xdiff = local_x - mean(local_x);
}

parameters {
  vector<lower=0,upper=100>[n]    q5;    // h = log(q5/100) in the Wilmoth et al. system
  vector[n]                       k;     // k = Wilmoth et al. shape parameter
  matrix<lower=-2,upper=2>[n,2]   beta;  // fertility shape parameters


  real<lower=0,upper=20>   mu;       // grand mean of TFR
  vector[n]                eta;      // deep parameters for (t,f,r) effects
  real                     betax;    // coeffs for local x-vars

  real<lower=0> sigma_t;      // sd of TFR across troncos
  real<lower=0> sigma_f;      // sd of TFR across families within tronco
  real<lower=0> sigma_r;      // sd of TFR across familie

}

transformed parameters {
  vector[n]             theta;      // expected TFR in a terra, given its family mixture
  vector[nt+nf+n]       eps;        //
  vector[nt+nf+n]       sigma_vec;  // long vector of 1/std devs 

  for (i in 1:nt)                sigma_vec[i] = sigma_t;
  for (i in (nt+1):(nt+nf))      sigma_vec[i] = sigma_f;
  for (i in (nt+nf+1):(nt+nf+n)) sigma_vec[i] = sigma_r;

// theta is a (#terra)-vector of expected TFRs, given cultural composition
// and literacy level

  theta = rep_vector(mu, n)
            + (include_x) * (xdiff * betax) 
            + eta;

   eps = (K * eta) ./ sigma_vec ; 

}

model {

  vector[n]             h;       // log(q5/100)
  matrix[n,8]           Fx;      // age-group fertility rates F10...F45  (F10=0)
  matrix[n,11]          mx;      // mortality rates for age groups starting at 0,1,5,10,...45
  matrix[n,12]          lx;      // life table {lx} values for 0,1,5...,50
  matrix[n,10]          Lx;      // life table {5Lx} values for 0,5...,45
  matrix[n,7]           Sx;      // life table {5Lx} values for 0,5...,45

  matrix[n,4]          tmp;
  matrix[n,11]         tmp2;

  matrix[7,n]           Kx;         // expected surv children 0-4 per woman in age group x to x+4
  vector[n]             expected_C; // expected surv total number of children       


  matrix[n,7] phi;     // proportion of total fertility by age group
  matrix[n,7] gamma;
  vector[n]   colsums_phi;

// fertility pattern

  gamma =  M + beta * X ;
  colsums_phi = exp(gamma) * rep_vector(1, 7);

  phi = diag_pre_multiply( 1 ./ colsums_phi, exp(gamma) );

  h = log(q5 ./ 100.);

  tmp[,1] = rep_vector(1.0, n);
  tmp[,2] = h;
  tmp[,3] = square(h);
  tmp[,4] = k;

  tmp2   = exp( tmp * wilmothABCV) ;
  mx     = tmp2;
  mx[,2] = -0.25 * tmp2[,1] - 0.25 * log(1- (q5 ./100));

  lx = exp( -mx * cs_constants) ;

  Lx = lx * trapez_constants;

  // numerator in Sx is Lx[, 10 ... 40], denominator is Lx[, 15 ... 45]
  Sx = block(Lx, 1, 3, n, 7 ) ./ block(Lx, 1, 4, n, 7 );  

  Fx = 0.20 * append_col( rep_vector(0.0, n), diag_pre_multiply( theta, phi));

  for (j in 1:7) {
    Kx[j,] = to_row_vector( (Sx[,j] .* Fx[,j] + Fx[,j+1]) .* Lx[,1] / 2. );
  }

  // expected # of children for each terra population, at current param values

  expected_C = (W .* Kx') * rep_vector(1.0,7);

  // DISTRIBUTIONAL ASSUMPTIONS

  // LIKELIHOOD
  C ~ poisson(expected_C);

  // PRIORS

  mu      ~ normal(5,2);

  eps   ~ normal(0, 1);

  target += -sum( log(sigma_vec));  // because eps is a non-linear transf of params

  betax ~ normal(0,5);

  beta[,1] ~ normal(0, 1);
  beta[,2] ~ normal(0, 1);

  // vectorized. 90% prior prob. that q5 is between 1/2 and 2x estimated q5
  q5 ./ 100.       ~ beta(QAB[,1], QAB[,2]);   

  k        ~ normal(0,1);

  sigma_t   ~ normal(0, 1);
  sigma_f   ~ normal(0, 1);
  sigma_r   ~ normal(0, 1);

}
