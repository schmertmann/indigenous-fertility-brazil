#-------------------------------------------------------
# read the terra and sector shapefiles from the geobr
# package, then save results for later use
#
# Problem 24 Jan 2020: the gid codes have changed. 
# The {geobr} package uses the new codes, but our 
# original analysis (eg, the TIpop dataframe) uses
# the old codes.  Only 201/457 of old codes have a 
# match in the {geobr} data.
#-------------------------------------------------------

rm(list=ls())

library(tidyverse)
library(geobr)
library(sf)

geobr_TI = read_indigenous_land(date=201907)

geobr_setores = read_census_tract(code_tract = 'all', year=2010)

save(geobr_TI, geobr_setores, file='../Data/geobr.Rdata')

