#######################################################################
# Stan version of C/W , using cultural hierarchical model
# Carl Schmertmann
# started 18 May 2017
# edited  22 Jan 2020
#
# 15 Oct: removes Terras with zero populations
# 16 Oct: slight efficiencies in calculations, incl transformed data{}
#         and sparse matrix calculation for expected_C
# 17 Oct: added 0/1 switches for excluding/including trunk, family, literacy effects
# 14 Apr: reverted to non-sparse version, now uses same H,Z,G notation
#         as the Germany mortality paper
#         https://www.overleaf.com/14341705bnfmsbyyfjby
# 14 Aug 18: new input populations that include "donut hole" sectors
# 17 Aug 18: new system for data indexing (should be much cleaner),
#            read in most data in pre-prepared form from 
#            data-for-Stan-analysis.Rdata, created by
#            program '1-assemble-data-for-Stan-analysis.R'
# 20 Aug 18: corrects the non-identifiability problems
#            by using a clever sets of basis funtions (Z, K) for
#            (trunk-, family-, and terra-)level effects:
#            eps = (eps_t, eps_f, eps_r ) = SIGMA.INV %*% Z %*% gamma
#
# 22 Jan 20: split stan program file into separate .stan file for 
#            easier editing
#######################################################################

graphics.off()
if (.Platform$OS.type == 'windows') windows(record=TRUE)
rm(list=ls() )

library(rstan)
library(tidyverse)
library(Matrix)

rstan_options(auto_write = TRUE)
options(mc.cores=4)

#-------------------------------------------
savefit = TRUE

nchains = 4
niter   = 2500
nwarmup = 500
nthin   = 4

inc_literacy = 1
#-------------------------------------------


uf.df = read.csv(text='
ufabb, ufcode,              ufname
RO,    11,           Rondônia
AC,    12,               Acre
AM,    13,           Amazonas
RR,    14,            Roraima
PA,    15,               Pará
AP,    16,              Amapá
TO,    17,          Tocantins
MA,    21,           Maranhão
PI,    22,              Piauí
CE,    23,              Ceará
RN,    24,Rio Grande do Norte
PB,    25,            Paraíba
PE,    26,         Pernambuco
AL,    27,            Alagoas
SE,    28,            Sergipe
BA,    29,              Bahia
MG,    31,       Minas Gerais
ES,    32,     Espírito Santo
RJ,    33,     Rio de Janeiro
SP,    35,          São Paulo
PR,    41,             Paraná
SC,    42,     Santa Catarina
RS,    43,  Rio Grande do Sul
MS,    50, Mato Grosso do Sul
MT,    51,        Mato Grosso
GO,    52,              Goáis
DF,    53,   Distrito Federal',
  stringsAsFactors=FALSE)

#------------- MORTALITY model ------------------

##--- Wilmoth et al. coefficients from Pop Studies
wilmoth =
  read.csv(text = '
           age,am,bm,cm,vm,af,bf,cf,vf
           0,  -0.5101, 0.8164,-0.0245,     0,-0.6619, 0.7684,-0.0277,     0
           1,      -99,    -99,    -99,   -99,    -99,    -99,    -99,   -99
           5,  -3.0435, 1.5270, 0.0817,0.1720,-2.5608, 1.7937, 0.1082,0.2788
           10, -3.9554, 1.2390, 0.0638,0.1683,-3.2435, 1.6653, 0.1088,0.3423
           15, -3.9374, 1.0425, 0.0750,0.2161,-3.1099, 1.5797, 0.1147,0.4007
           20, -3.4165, 1.1651, 0.0945,0.3022,-2.9789, 1.5053, 0.1011,0.4133
           25, -3.4237, 1.1444, 0.0905,0.3624,-3.0185, 1.3729, 0.0815,0.3884
           30, -3.4438, 1.0682, 0.0814,0.3848,-3.0201, 1.2879, 0.0778,0.3391
           35, -3.4198, 0.9620, 0.0714,0.3779,-3.1487, 1.1071, 0.0637,0.2829
           40, -3.3829, 0.8337, 0.0609,0.3530,-3.2690, 0.9339, 0.0533,0.2246
           45, -3.4456, 0.6039, 0.0362,0.3060,-3.5202, 0.6642, 0.0289,0.1774
           50, -3.4217, 0.4001, 0.0138,0.2564,-3.4076, 0.5556, 0.0208,0.1429
           55, -3.4144, 0.1760,-0.0128,0.2017,-3.2587, 0.4461, 0.0101,0.1190
           60, -3.1402, 0.0921,-0.0216,0.1616,-2.8907, 0.3988, 0.0042,0.0807
           65, -2.8565, 0.0217,-0.0283,0.1216,-2.6608, 0.2591,-0.0135,0.0571
           70, -2.4114, 0.0388,-0.0235,0.0864,-2.2949, 0.1759,-0.0229,0.0295
           75, -2.0411, 0.0093,-0.0252,0.0537,-2.0414, 0.0481,-0.0354,0.0114
           80, -1.6456, 0.0085,-0.0221,0.0316,-1.7308,-0.0064,-0.0347,0.0033
           85, -1.3203,-0.0183,-0.0219,0.0061,-1.4473,-0.0531,-0.0327,0.0040
           90, -1.0368,-0.0314,-0.0184,     0,-1.1582,-0.0617,-0.0259,     0
           95, -0.7310,-0.0170,-0.0133,     0,-0.8655,-0.0598,-0.0198,     0
           100,-0.5024,-0.0081,-0.0086,     0,-0.6294,-0.0513,-0.0134,     0
           105,-0.3275,-0.0001,-0.0048,     0,-0.4282,-0.0341,-0.0075,     0
           110,-0.2212,-0.0028,-0.0027,     0,-0.2966,-0.0229,-0.0041,     0
           ')

af = wilmoth$af[1:11]  # keep age 0,1,...45
bf = wilmoth$bf[1:11]  # keep age 0,1,...45
cf = wilmoth$cf[1:11]  # keep age 0,1,...45
vf = wilmoth$vf[1:11]  # keep age 0,1,...45

###############################################################
# load pre-prepared input data 
###############################################################

# TI population characteristics, H and Z matrices, etc
load('../Data/AA-data-for-Stan-analysis.Rdata')

Children = TIpop$C0
Women    = as.matrix( TIpop[,paste0('F',seq(15,45,5))])


# FERTILITY model calibrated constants -------
load('../Data/svd.constants.RData')   # m and X

# construct the matrix of constants for cumulative hazard calculations
n            = c(1,5, rep(5,9))    # widths of life table age intervals for x=0,1,5,10...45
cs_constants = matrix(0, 11, 12)
for (j in 1:11) cs_constants[1:j,j+1] = head(n,j)

# construct the constants for the trapezoidal approx of L0...L45 from a row of l0,l1,l5,l10,...,l50
trapez_constants = matrix(0, 12, 10,
                          dimnames=list(paste0('l', c(0,1,seq(5,50,5))), paste0('L', seq(0,45,5))))
trapez_constants[c('l0','l1','l5'), 'L0'] = c( 1/2, 5/2, 4/2)
for (j in 2:10) trapez_constants[j+1:2, j] = 5/2


####################################################    
  #------------ STAN DATA  --------------------
  stanDataList = list(
    n       = nterra,                  # number of terras

    nt  = ntrunk,                # number of trunco effects
    nf  = nfamily,                # number of families
    nx  = 1,

    local_x = TIpop$alfa1534,  

    C = Children,
    W = Women,
    
    QAB = QAB,   # nterra x 2

    K = K, # (ntrunk+nfamily+nterra) x (nterra)

    wilmothABCV = rbind(af,bf,cf,vf),
    cs_constants = cs_constants,
    trapez_constants = trapez_constants,

    M = matrix(svd.constants$m, nrow=nterra, ncol=7, byrow=TRUE),

    X = svd.constants$X,

    include_x = inc_literacy

  )

stanInits = function(nchains=1) {
  L = vector('list',nchains)

  n   = nterra
  nt  = stanDataList$nt
  nf  = stanDataList$nf
  nx  = stanDataList$nx
  QAB = stanDataList$QAB

  for (i in seq(L)) {
    L[[i]] =   list(

      q5 = 100*rbeta(n=nterra, shape1=QAB[,1], shape2=QAB[,2]),
      k = rnorm(n=nterra, mean=0,sd=1),
      beta = matrix(runif(nterra*2, min=-.10, max=.10) , nterra,2),
      mu  = runif(n=1, min=2, max=10),
      eta = runif(n, 2, 6),

      betax       = rnorm(1, 0, 2),

      sigma_t   = runif(1,0.1, 0.5),
      sigma_f   = runif(1,0.1, 0.5),
      sigma_r   = runif(1,0.1, 2.0)

        )
  }
  return(L)
} # stanInits


  MODEL = stan_model(file      = 'cultural-hierarchy.stan',
                     model_name='Indigenas')


  fit = sampling(
                 MODEL,
                 data=stanDataList,
                 pars       = c('mu','theta','betax',
                                'sigma_t','sigma_f', 'sigma_r',
                                'eta',
                                'q5','k','beta'),
                 init       = stanInits(nchains),
                 iter       = niter,
                 warmup     = nwarmup,
                 thin       = nthin,
                 chains     = nchains,
                 control = list(stepsize=.05, 
                                adapt_delta=.95, 
                                max_treedepth=10)
                 )


########################################################
# Summarize and save MCMC output
########################################################

# indices for rows of Z that correspond to trunk, family, region effects
tix = 1:ntrunk
fix = ntrunk + 1:nfamily
rix = ntrunk + nfamily + 1:nterra

#-----------------
# trunk effects
#-----------------
  
ttable = data.frame(
            trunk = colnames(Ht),
            teff  = K[tix,] %*% get_posterior_mean(fit, 'eta')[,1+nchains]
           ) 

# add quantiles
Q = t( apply( K[tix,] %*% t( as.matrix(fit,'eta')), 1,  quantile,
              probs=c(.10,.25,.50,.75,.90)) )

ttable       = data.frame( ttable, Q)
names(ttable) = c('trunk','teff',paste0('Q',c(10,25,50,75,90)))

ttable = ttable %>% 
         mutate_at(vars(teff:Q90), round, 2)

#-----------------
# family effects
#-----------------

ftable = data.frame(
  family = colnames(Hf),
  feff  = K[fix,] %*% get_posterior_mean(fit, 'eta')[,1+nchains]
) 

# add quantiles
Q = t( apply( K[fix,] %*% t( as.matrix(fit,'eta')), 1,  quantile,
              probs=c(.10,.25,.50,.75,.90)) )

ftable       = data.frame( ftable, Q)
names(ftable) = c('family','feff',paste0('Q',c(10,25,50,75,90)))

ftable = ftable %>% 
  mutate_at(vars(feff:Q90), round, 2)

#-----------------
# summary of various
# effects for terras
#-----------------

rtable = data.frame(
  terraname = rownames(Hf),
  GID0      = TIpop$GID0,
  iTFR      = 7 * TIpop$C0/TIpop$W,
  lit       = TIpop$alfa1534,
  mu        = get_posterior_mean(fit,'mu')[1+nchains],
  teff      = Ht %*% K[tix,] %*% get_posterior_mean(fit,'eta')[,1+nchains],
  feff      = Hf %*% K[fix,] %*% get_posterior_mean(fit,'eta')[,1+nchains],
  culture   = NA,
  liteff    = (TIpop$alfa1534 - mean(TIpop$alfa1534)) * get_posterior_mean(fit,'betax')[1+nchains],
  reff      = K[rix,] %*% get_posterior_mean(fit,'eta')[,1+nchains],
  theta     = NA,
  row.names = NULL
) %>%
   mutate( culture = teff + feff,
           theta = mu + liteff + culture + reff)


rtable = rtable %>% 
  mutate_at(vars(iTFR:theta), round, 3)



if (savefit) {
  fname = paste0('../Data/PNAS-fit',format(Sys.time(), '-%Y-%m-%d-%H%M')  ,'.RData')
  save(fit, TIpop, ttable, ftable, rtable, file=fname)
}


