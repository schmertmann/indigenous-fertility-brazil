########################################################
# Carl 
# created 16 Aug 18
# altered 25 Aug 18
#
# construct the datasets and tables necessary for
# estimating the Stan cultural hierarchy model
#
# INPUTS
# (1) IBGE population data in file setores.indigenas.csv
# (2) cultural data for Terras Indigenas from the
#     spreadsheet created by Marta and students at UNICAMP 
#
# OUTPUTS (saved in data-for-Stan-analysis.Rdata)
#  (1) TIpop dataframe (population structure and literacy info by Terra)
#  (2) QAB ([a,b] beta parameters for q5 in each Terra)
#  (3) 
########################################################

rm(list=ls())

library(dplyr)
library(ggplot2)
library(stringr)

# the two input datasets should have the same number of rows
# (one per included sector). The population data is constructed
# in program 'construct.setores.indigenas.R'.

# population by sector, for the sectors that overlap indigenous territories
population = read.csv('../Data/setores.indigenas.csv', header=TRUE, stringsAsFactors = FALSE,
                      encoding = 'UTF-8')

# cultural information by sector, for the sectors that overlap indigenous territories
culture    = read.csv('../Data/planilhona-official.csv', header=TRUE, stringsAsFactors = FALSE,
                      encoding = 'UTF-8')


#############################################################
# aggregate population to TI level (GID0 is the terra index)
# Note that some TIs will have enumerated populations equal to
# zero; we'll eliminate them now
#############################################################

TIpop = population %>%
          group_by(GID0, terraname=NOME_TI23) %>%
          summarize_at(vars(C0:AF45), sum, na.rm=TRUE ) %>%
          mutate( W=F15+F20+F25+F30+F35+F40+F45) %>%
          filter(W>0) %>%
          mutate(alfa1534 = (AF15+AF20+AF25+AF30)/(F15+F20+F25+F30))

#############################################################
# for each TI (=each row of TIpop), create a list of the
# language trunks and families that are represented among that TI's 
# residents
#############################################################

# list of all possible language families
family.vals = c("Aikana", "Arawa", "Arikem", "Aruak", "Aweti", 
                "Bora", "Bororo", "Chiquito", "Guaikuru", "Guato", 
                "Iranxe", "isolados", "Jabuti", "Je", "Juruna", 
                "Kaingang", "Kanoe", "Karaja", "Karib", "Katukina", 
                "Krenak", "Kwaza", "Maku", "Mawe", "Maxakali", 
                "Monde", "Munduruku", "Mura", "Nambikwara", "Ofaye", 
                "Pano", "Paumari", 
                
                "portugues-Arara", "portugues-Atikum", 
                "portugues-Jenipapo-Kaninde", "portugues-Jeripanco", 
                "portugues-Kaimbe", "portugues-Kaixana", 
                "portugues-Kambiwa", "portugues-Kantarure", 
                "portugues-Kapinawa", "portugues-Karapoto", 
                "portugues-Kariri-Xoco", "portugues-Kiriri", 
                "portugues-Miranha", "portugues-Mura", 
                "portugues-Pankarare", "portugues-Pankararu", 
                "portugues-Pataxo", "portugues-Pataxo Ha Ha Hae", 
                "portugues-Pipipa", "portugues-Pitaguari", 
                "portugues-Potiguara", "portugues-Tabajara", 
                "portugues-Tapeba", "portugues-Tapuia", 
                "portugues-Tingui Boto", "portugues-Tremembe", 
                "portugues-Truka", "portugues-Tumbalala", 
                "portugues-Tupiniquim", "portugues-Tuxa", 
                "portugues-Wassu", "portugues-Xoko", 
                "portugues-Xukuru", "portugues-Xukuru-Kariri", 
                
                "Ramarama", "Rikbaktsa", "Tikuna", "Trumai", "Tukano", 
                "Tupari", "Tupi-Guarani", "Txapakura", "Witoto", "Yanomami", 
                "Yate")

# list of trunks for each of the language families
trunk.vals = c("Aikana", "Arawa", "Tupi", "Aruak", "Tupi", 
               "Bora", "Macro-Je", "Chiquito", "Guaikuru", "Macro-Je", 
               "Iranxe", "isolados", "Jabuti", "Macro-Je", "Tupi", 
               "Macro-Je", "Tupi", "Macro-Je", "Karib", "Katukina", 
               "Krenak", "Kwaza", "Maku", "Tupi", "Macro-Je", 
               "Tupi", "Tupi", "Mura", "Nambikwara", "Macro-Je", 
               "Pano", "Paumari", 
               
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              "Portugues", "Portugues", 
              
              "Tupi", "Macro-Je", "Tikuna", "Trumai", "Tukano", 
              "Tupi", "Tupi", "Txapakura", "Witoto",  "Yanomami", 
              "Macro-Je")

# abbreviate into 4 trunks
main.trunks = c('Tupi','Macro-Je','Portugues')
trunk.vals[ !(trunk.vals %in% main.trunks) ] = 'Other'

names(trunk.vals) = family.vals
head(trunk.vals,10)

### for each terra id, create a vector of character values for the 
### residents' language families

GID0.vals = unique(TIpop$GID0)

families_represented = 
  lapply( GID0.vals, function(this.gid) {
           tmp = filter(culture, GID0==this.gid)$família.linguística.IBGE.Funai
           strsplit( trimws( str_replace_all(tmp, ", " , ','), which='both'), ',')[[1]]           
}
)

## translate from families -> abbreviated trunks (only 4 trunks)
trunks_represented = sapply( families_represented, function(fname) {
                                unique( unname(trunk.vals[fname]))
})

save( families_represented, trunks_represented, file='../Data/families and trunks.Rdata')

########################################################################
# construct the H and G matrices for the cultural hierarchy effects
#########################################################################

unique_families_represented = unique( unlist( families_represented))
unique_trunks_represented   = unique( unlist( trunks_represented))

nterra  = nrow(TIpop)  # number of terras with non-zero populations
nfamily = length( unique_families_represented )
ntrunk  = length( unique_trunks_represented )

# Ht is nterra x ntrunk, row-stdized:  which families are in which terras? 
Ht = matrix(0, nterra, ntrunk, 
            dimnames=list(TIpop$terraname, unique_trunks_represented))

for (i in seq(trunks_represented)) {
  these.trunks = trunks_represented[[i]]
  Ht[i, these.trunks ] = 1/length(these.trunks)
}


# Hf is nterra x nfamily, row-stdized:  which families are in which terras? 
Hf = matrix(0, nterra, nfamily, 
            dimnames=list(TIpop$terraname, unique_families_represented))

for (i in seq(families_represented)) {
  these.families = families_represented[[i]]
  Hf[i, these.families ] = 1/length(these.families)
}

Gt = matrix( 1, 1, ntrunk)   # one restriction: trunk effects sum to zero


# ntrunk restrictions: family effects sum to zero within each trunk
G = matrix( 0, ntrunk, nfamily,
            dimnames=list( unique_trunks_represented, unique_families_represented))
for (ff in unique_trunks_represented) {
  candidate_families = names( which(trunk.vals == ff))
  keeper_families    = intersect( candidate_families, colnames(G)) 
  G[ff, keeper_families ] = 1
}

Gf = G

# 1 restriction: region effects sum to zero
Gr = matrix( 1, 1, nterra)

## calculate the big basis matrix 
##
##  Ht  Hf  I
##  Gt   0  0
##   0  Gf  0
##   0   0 Gr

big1 = rbind( Ht, 
              Gt, 
              diag(0,ncol(Ht)),
              matrix(0,1,ncol(Ht)))
big2 = rbind( Hf, 
              matrix(0,1,ncol(Hf)), 
              Gf,
              matrix(0,1,ncol(Hf)))
big3 = rbind( diag(nterra),
              matrix(0,1,nterra),
              matrix(0,4,nterra),
              Gr)

BIG = cbind(big1, big2, big3)

library(MASS)  # for generalized inverse


# columns of Z are basis functions for (trunk, family, terra)-level
# effects on TFR
#

# Z is (ntrunk+nfamily+nterra) x (nterra), probably 532 x 457
#   Z * eta = the minimum-length vector of (trunk, family, region)
# effects (ut, uf, ur) such that Ht*ut + Hf*uf + ur = eta  
#

K = ginv(crossprod(BIG)) %*% t(BIG[1:nterra,])


################################################################
# estimate the (a,b) parameters for the q5 distribution in 
# each terra.  These are based on the estimates for all residents
# of the municipios that overlap the terras. 
#
# For example, if Terra #16 is part of municipios A, B, and C that
# have Atlas do Desenv Humano estimates of (16, 8, and 23) for q5 per 1000,
# then we want q5 ~ Beta(a,b) such that 
# P[q5 < (min(16,8,23))/2 ] = .05 and 
# P[q5 ? (max(16,8,23))*2 ] = .05 
################################################################

# Load Human Dev Atlas and geographic data
atlas = read.csv('../Data/atlas.csv', stringsAsFactors = FALSE)

str(atlas)
head(atlas)

#--- for each terra, look up the corresponding municipios in the Atlas do DH,
#--- and record the range of q5 values

q5_range = 
  lapply( GID0.vals, function(this.gid) {
     these.codes = unique( filter(population, GID0==this.gid)$municode )
     range( filter( atlas, municode %in% these.codes)$MORT5 / 1000 )
  }
)

QAB = sapply( q5_range, function(Q) {
  LearnBayes::beta.select( list(p=.05, x=0.5*Q[1]),
                           list(p=.95, x=2.0*Q[2]))
})

QAB = t(QAB)

#########################################################################
# save all of the processed data for use by Stan program
#########################################################################

#####################################

save( nterra, ntrunk, nfamily, 
      TIpop, QAB, 
      K, Ht, Gt, Hf, Gf, 
      families_represented, trunks_represented,
      file='../Data/AA-data-for-Stan-analysis.Rdata')


